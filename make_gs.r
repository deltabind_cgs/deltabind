### Construct ground truth set from Multi-factor GEM and edgeR outputs END

nd2.edger = nd2
nd2.edgerpv = tf.e2sorted$PValue[nd2.edger]
nd2.edger.p.05 = (which(tf.e2sorted$PValue>=0.05))[1]
tf.e2sorted.full = tf.e2sorted
tf.e2sorted = tf.e2sorted[1:nd2.edger.p.05,]
kset_start = 0.6; kset_end = 20
kset = seq(kset_start,kset_end,by=0.4)
n.e2 = rbind(n.chipe2 = rep(-1, length(kset)), n.piqe2 = rep(-1, length(kset)))
for (i in 1:length(kset)) {
  k = kset[i]
  nd2 = floor(k*nd2.edger); nd2 = min(nd2, nrow(tf.e2sorted))
  edger.qdiff.e2.ovid = which(tf.e2sorted$E1_Q[1:(nd2)] <2.5 & tf.e2sorted$E2_Q[1:(nd2)]>pmax(2.5, 0.5+tf.e2sorted$E1_Q[1:(nd2)]) )
  n.chipe2 = length(edger.qdiff.e2.ovid)
  tf.e2chr=tf.e2sorted$chr[1:nd2]
  tf.e2coord=tf.e2sorted$coord[1:nd2]
  tfgr.edgerqdiffe2ovid = GRanges(tf.e2chr[edger.qdiff.e2.ovid], ranges=IRanges(start=tf.e2coord[edger.qdiff.e2.ovid]-flank, end=tf.e2coord[edger.qdiff.e2.ovid]+flank))
  
  gs.edgerqdiff.e2.piqidx = unique(subjectHits(findOverlaps(tfgr.edgerqdiffe2ovid, tfgr.piq)))
  n.piqe2 = length(gs.edgerqdiff.e2.piqidx)
  n.e2[,i] = c(n.chipe2, n.piqe2)
  if (i>3) {
    if (n.e2['n.piqe2',i-2]==n.e2['n.piqe2',i-1] & n.e2['n.piqe2', i-1]==n.e2['n.piqe2',i]) {
      kset_end = k
      kset = seq(kset_start, kset_end, by=0.4)
      n.e2 = n.e2[,1:i]
      break
    }
  }
}
smsp.piqe2 = smooth.spline(kset, n.e2['n.piqe2',], df=min(20, length(kset)-2))
derivx = seq(1,kset_end, by=0.1)
spfit.piqe2deriv = (predict(smsp.piqe2, x=derivx, deriv=1))$y
spfit.piqe2deriv[spfit.piqe2deriv<0]=0
k = derivx[which(spfit.piqe2deriv<=spfit.piqe2deriv[1]/1.6)[1]]
if (is.na(k)){
  k = kset_end
}

nd2 = floor(k*nd2.edger); nd2 = min(nd2, nrow(tf.e2sorted))
edger.qdiff.e2.ovid = which(tf.e2sorted$E1_Q[1:(nd2)] <2.5 & tf.e2sorted$E2_Q[1:(nd2)]>pmax(2.5, 0.5+tf.e2sorted$E1_Q[1:(nd2)]) )
length(edger.qdiff.e2.ovid)
tf.e2chr=tf.e2sorted$chr[1:nd2]
tf.e2coord=tf.e2sorted$coord[1:nd2]
tfgr.edgerqdiffe2ovid = GRanges(tf.e2chr[edger.qdiff.e2.ovid], ranges=IRanges(start=tf.e2coord[edger.qdiff.e2.ovid]-flank, end=tf.e2coord[edger.qdiff.e2.ovid]+flank))

gs.edgerqdiff.e2.piqidx = unique(subjectHits(findOverlaps(tfgr.edgerqdiffe2ovid, tfgr.piq)))
