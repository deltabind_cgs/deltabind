# Calculate PR and ROC metrics
call.prpts = sapply(callsets.comb, function(s){
  pr = calc_pr_e2_tf(s, gs.edgerqdiff.e2.piqidx)
})
p_at_selected_r = approx(call.prpts['r',], call.prpts['p',], c(.05, .1, .15, .2))


call.aupr = calc_aupr(call.prpts)

# Plot PR curve
plot(base.prpts['r',], base.prpts['p',], lty=4, type='l', xlim=c(0,1), ylim=c(0,1), main=paste(tfname, 'PR'),
     xlab='Recall', ylab='Precision')
grid(lwd=3)
points(call.prpts['r',], call.prpts['p',], type='l')
legend(x='topright', legend=c('deltaDNase','PIQ diff'), lty=c(1,4), cex=0.8)

# Calculate AUC
call.auc = calc_auc(call.prpts)
# Plot ROC curve
plot(base.prpts['fpr',], base.prpts['r',], lty=4, type='l', xlim=c(0,1), ylim=c(0,1), main=paste(tfname, 'ROC'),
     xlab='FPR', ylab='TPR')
grid(lwd=3)
points(call.prpts['fpr',], call.prpts['r',], type='l')
legend(x='bottomright', legend=c('deltaDNase','PIQ diff'), lty=c(1,4), cex=0.8)
