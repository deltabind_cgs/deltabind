## DeltaBind

DeltaBind infers differential binding events from single-condition binding scores for each condition. Requires two replicate experiments for the “bound” condition and at least one replicate for the “unbound” condition. 

## How to run

cd to `deltabind` directory, specify `tfid` in `deltabind_main.r`. `tfid` can be looked up in `tfinfo.txt`. Then

```
Rscript deltabind_main.r
```
