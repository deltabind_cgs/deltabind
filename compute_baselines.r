##############
# Baseline
##############

# Function for calculating precision-recall curve points
calc_pr_e2_tf = function(idx, gsidx) {
  ntp=length(intersect(idx, gsidx))
  nq=length(idx)
  np=length(gsidx)
  nn=npiq-np
  # PR
  p=ntp / nq                                      
  r=ntp / np
  # ROC
  fpr = (nq-ntp)/nn
  c(p=p,r=r,ntp=ntp,nq=nq,np=np,fpr=fpr)
}

# Calculate AUPR and AUROC
calc_aupr = function(prpts){
  p = prpts['p',]
  p_notnan = which(!is.nan(p))[1]
  if (p_notnan>1) { p[1:(p_notnan-1)] = p[p_notnan] }
  r = prpts['r',]
  stopifnot(sum(r!=sort(r))==0); stopifnot(length(p)==length(r) & length(r)>0)
  p = c(p[1], p, p[length(p)])
  r = c(0, r, 1)
  aupr = 0
  for (i in seq(2, length(r))){
    aupr = aupr + (r[i]-r[i-1])*(p[i]+p[i-1])/2
  }
  aupr
}

calc_auc = function(prpts){
  r = prpts['r',]
  fpr = prpts['fpr',]
  stopifnot(sum(fpr!=sort(fpr))==0); stopifnot(length(fpr)==length(r) & length(fpr)>0)
  r = c(0, r, 1)
  fpr = c(0, fpr, 1)
  auc = 0
  for (i in seq(2, length(fpr))){
    auc = auc + (fpr[i]-fpr[i-1])*(r[i]+r[i-1])/2
  }
  auc
}

al1 = seq(from = min(tf.kc1$shape - tf.gc1$shape), to = max(tf.kc1$shape - tf.gc1$shape), length.out = 30)
al2 = quantile(tf.kc1$shape-tf.gc1$shape, probs=seq(1,0,length.out=15))
al = sort(c(al1,al2), decreasing = T)
base.prpts = sapply(al, function(aa){
  idx = which(tf.kc1$shape - tf.gc1$shape >= aa)
  pr = calc_pr_e2_tf(idx, gs.edgerqdiff.e2.piqidx)
})

gc1readscore = tf.gc1$readscore; gc1readscoresum = sum(gc1readscore)
kc1readscore = tf.kc1$readscore; kc1readscoresum = sum(kc1readscore)
kc1readscore = kc1readscore * gc1readscoresum / kc1readscoresum
readscorediff = kc1readscore - gc1readscore
al3 = quantile(readscorediff, probs=c(.9999, .999, seq(.99,0,by=-0.02)))
rdct.prpts = sapply(al3, function(aa){
  idx = which(readscorediff >=aa)
  pr = calc_pr_e2_tf(idx, gs.edgerqdiff.e2.piqidx)
})


p_at_10pc_base = approx(base.prpts['r',], base.prpts['p',], .1)
p_at_10pc_rdct = approx(rdct.prpts['r',], rdct.prpts['p',], .1)
base.aupr = calc_aupr(base.prpts)
rdct.aupr = calc_aupr(rdct.prpts)

base.auc = calc_auc(base.prpts)
rdct.auc = calc_auc(rdct.prpts)