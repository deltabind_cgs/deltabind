
##############
# Generate output graphics
##############
png(paste0(outdir,tfid,'-',tfname,'-boundary.png'))
plot(tf.gc.srk, tf.kc.srk, pch='.', xlab='GM12878 PIQ rank', ylab='K562 PIQ rank', main=tfname)
points(tf.gc.srk[gs.edgerqdiff.e2.piqidx], tf.kc.srk[gs.edgerqdiff.e2.piqidx], col='red', pch='*')
for (i in c(8,9,10,12,14,16,19,22,25,28)){
  points(frontiers[[i]], allqc*npiq, type='l', col='orange', lwd=2)
}
dev.off()

png(paste0(outdir,tfid,'-',tfname,'-PR.png'))
plot(base.prpts['r',], base.prpts['p',], lty=4, type='l', xlim=c(0,1), ylim=c(0,1), main=paste(tfname, 'PR'),
     xlab='Recall', ylab='Precision')
grid(lwd=3)
points(call.prpts['r',], call.prpts['p',], type='l')
legend(x='topright', legend=c('deltaDNase','PIQ diff'), lty=c(1,4), cex=0.8)
dev.off()

png(paste0(outdir,tfid,'-',tfname,'-ROC.png'))
plot(base.prpts['fpr',], base.prpts['r',], lty=4, type='l', xlim=c(0,1), ylim=c(0,1), main=paste(tfname, 'ROC'),
     xlab='FPR', ylab='TPR')
grid(lwd=3)
points(call.prpts['fpr',], call.prpts['r',], type='l')
legend(x='bottomright', legend=c('deltaDNase','PIQ diff'), lty=c(1,4), cex=0.8)
dev.off()

# rounding function
rd = function(x){round(x,digits=3)}

tfsummary = data.frame(tfid=tfid, n_chip_diff=length(gs.edgerqdiff.e2.piqidx), npiq=npiq, db.auc=rd(call.auc), base.auc=rd(base.auc), rdct.auc=rd(rdct.auc),
                       db.p_at_r_0.1=rd(p_at_selected_r$y[2]), base.p_at_r_0.1=rd(p_at_10pc_base$y), rdct.p_at_r_0.1=rd(p_at_10pc_rdct$y),
                       db.aupr=rd(call.aupr), base.aupr=rd(base.aupr), rdct.aupr=rd(rdct.aupr))
write.csv(tfsummary, file = paste0(outdir,tfid,'-',tfname,'-summary.csv'), quote=F, row.names = F)
