# deltabind.r
tfid = 3

require(mvtnorm)
require(GenomicRanges)

# Change working directory to "deltabind_code"
outdir = './deltabind_output/'
ntf = 20
  print ('*************************************************')
  print(tfid)
  if(tfid==8 | tfid==16 | tfid > ntf | tfid < 1){stop()}
  load(paste0('tf.deltabind.rdata/deltabind-',tfid,'-data.RData'))
  npiq = length(tf.kc1$shape)

  
  
  ### Construct ground truth set from Multi-factor GEM and edgeR outputs
  source('make_gs.r')
  
  tfstr = paste0(tfid,'-',tfname)
  filestr = paste0(tfid,'-',tfname,'-diag')
  pdf(paste0(outdir, filestr, '.pdf'))
  par(mfrow=c(1,1))
  
  # Data plots
  plot(tf.gc1$shape, tf.kc1$shape, pch='.', main=paste(tfstr, 'DHS candidate sites and ChIP differential sites (red)'))
  points(tf.gc1$shape[gs.edgerqdiff.e2.piqidx], tf.kc1$shape[gs.edgerqdiff.e2.piqidx], pch='*', col='red')

  plot(rank(tf.gc1$shape), rank(tf.kc1$shape), pch='.', main=paste(tfstr, 'DHS candidate sites and ChIP differential sites (red)'))
  points(rank(tf.gc1$shape)[gs.edgerqdiff.e2.piqidx], rank(tf.kc1$shape)[gs.edgerqdiff.e2.piqidx], pch='*', col='red')
  
  plot(rank(tf.kc1$shape), rank(tf.kc3$shape), pch='.', main=paste(tfstr, 'DHS candidate sites and ChIP differential sites (red)'))
  points(rank(tf.kc1$shape)[gs.edgerqdiff.e2.piqidx], rank(tf.kc3$shape)[gs.edgerqdiff.e2.piqidx], pch='*', col='red')
  plot(rank(tf.gc1$shape), rank(tf.gc3$shape), pch='.', main=paste(tfstr, 'DHS candidate sites and ChIP differential sites (red)'))
  points(rank(tf.gc1$shape)[gs.edgerqdiff.e2.piqidx], rank(tf.gc3$shape)[gs.edgerqdiff.e2.piqidx], pch='*', col='red')
  
  
  # Compute baseline metrics
  source('compute_baselines.r')
  

  ######################
  # Main method
  ######################
  tf.kc1.srk=rank(tf.kc1$shape); tf.kc3.srk=rank(tf.kc3$shape); tf.gc1.srk=rank(tf.gc1$shape); tf.gc3.srk=rank(tf.gc3$shape)
  
  allqc = c(.2, .4, .5, .6, .65, .7, .75, .8, .82, .84, .86, .88, .90, .91, .92, .93, .94, .95, .96, .97, .98, .99, .995, .999, .9995, .9999)
  
  ## IDR
  tf.kc1.qnorm = qnorm((tf.kc1.srk+1) / (npiq+2)); tf.kc3.qnorm = qnorm((tf.kc3.srk+1)/(npiq+2))
  set.seed(1); idrsamp = sample(1:npiq, 15000)
  xdata = cbind(tf.kc1.qnorm[idrsamp], tf.kc3.qnorm[idrsamp])
  # EM iterations
  myEM <- function(x){
    # initialize
    c1 = 0.8; c2 = 1-c1
    u1 = c(-0.5, -0.5)
    sig1 = matrix(c(1,0,0,1), ncol=2)
    u2 = c(1,1)
    sig2 = matrix(c(1,0,0,1), ncol=2)
    
    iter = 0; niter=95
    outv = rep(-100,niter)
    while (iter < niter){
      iter = iter+1
      
      dn1 = dmvnorm(x, mean=u1, sigma=sig1)
      dn2 = dmvnorm(x, mean=u2, sigma=sig2)
      rn1 = c1*dn1 / (c1*dn1 + c2*dn2)
      rn2 = c2*dn2 / (c1*dn1 + c2*dn2)
      
      u1 = rn1 %*% x / sum(rn1)
      u2 = rn2 %*% x / sum(rn2)
      
      sig1.d1 = rn1 %*% (x[,1]-u1[1])^2 / sum(rn1)
      sig1.d2 = rn1 %*% (x[,2]-u1[2])^2 / sum(rn1)
      sig1 = diag(c(sig1.d1, sig1.d2))
      
      s2v = t(x)-as.vector(u2)
      sig2 = (s2v) %*% (rn2*t(s2v)) / sum(rn2)
      
      c1 = sum(rn1)/(sum(rn1)+sum(rn2))
      c2 = 1-c1
      outv[iter]=sig2[2,2]
    }
    u = eigen(sig2)    # check eigen vector
    if (u$vector[1,1]*u$vector[2,1] <= 0) {
      cat('Signal cluster not positively correlated.')
      stop()
    }
    list(u1=u1, u2=u2, sig1=sig1, sig2=sig2, c1=c1, c2=c2)
  }
  my.em.clus = myEM(xdata)
  # plot fitted EM clusters
  em1icov = solve(my.em.clus$sig1)
  em2icov = solve(my.em.clus$sig2)
  em1dens = function(x1, x2){ a = as.matrix(cbind(x1-my.em.clus$u1[1], x2-my.em.clus$u1[2])); diag(a%*%em1icov%*%t(a))}
  em2dens = function(x1, x2){ a = as.matrix(cbind(x1-my.em.clus$u2[1], x2-my.em.clus$u2[2])); diag(a%*%em2icov%*%t(a))}
  plotmyem = function(){
    plot(tf.kc1.qnorm, tf.kc3.qnorm, pch='.', main='IDR clusters')
    points(my.em.clus$u1[1], my.em.clus$u1[2], pch='+', col='orange')
    points(my.em.clus$u2[1], my.em.clus$u2[2], pch='+', col='red')
    px = seq(-4,4,by=0.1)
    py = px
    z1 = outer(px,py,em1dens)
    z2 = outer(px,py,em2dens)
    contour(px, py, z1, levels = c(4), add=T, col='orange', lwd=3)
    contour(px, py, z2, levels = c(4), add=T, col='red', lwd=3)
  }
  plotmyem()
  par(mfrow=c(1,1))
  
  ## Compute P(Ai| Ri)
  compute.p.a.given.r <- function(qx, psdo.ct = 0.0001){
    A = matrix(c(1,1,0,1), nrow=2)
    newm1 = A %*% as.vector(my.em.clus$u1)
    newm2 = A %*% as.vector(my.em.clus$u2)
    newcov1 = A %*% my.em.clus$sig1 %*% t(A)
    newcov2 = A %*% my.em.clus$sig2 %*% t(A)
    c1 = my.em.clus$c1
    c2 = my.em.clus$c2
    evalx = qnorm(qx)*2
    marg.clus1 = dnorm(evalx, newm1[2], sqrt(newcov1[2,2]))*c1 + psdo.ct
    marg.clus2 = dnorm(evalx, newm2[2], sqrt(newcov2[2,2]))*c2 + psdo.ct
    p.a.given.r = marg.clus2 / (marg.clus2 + marg.clus1)
    
    minIdx_p.a = which.min(p.a.given.r)
    maxIdx_p.a = which.max(p.a.given.r)
    p.a.given.r[1:minIdx_p.a] = p.a.given.r[minIdx_p.a]
    p.a.given.r[maxIdx_p.a:length(p.a.given.r)] = 
      (approx(c(qx[maxIdx_p.a],1),c(p.a.given.r[maxIdx_p.a],1),qx[maxIdx_p.a:length(qx)]))$y
    p.a.given.r = p.a.given.r*(1/p.a.given.r[length(p.a.given.r)])
  }
  p.a.given.r = compute.p.a.given.r(allqc)
  plot(allqc, p.a.given.r, main='P(Ai | Ri)', ylim=c(0,1), type='b'); grid(lwd=4)
  
  ## Compute P(Bi| Ai, Ri)
  # To improve stability for this term, find modes of rank difference for multiple (Ai, Ri) blocks, then fit a smooth spline approximation of modes w.r.t. Ri.
  testqc = seq(0.01, 0.99, by=0.02)
  testclus = sapply(testqc, function(qc){
    rc = qc*npiq
    del = 0.05*npiq
    iblock = which(abs(tf.kc1.srk-rc)<del & abs(tf.kc3.srk-rc)<del)
    rdif = (tf.gc1.srk[iblock] + tf.gc3.srk[iblock] - tf.kc1.srk[iblock] - tf.kc3.srk[iblock]) / 2
    denrdif = density(rdif)
    
    nrdif = length(rdif)
    mu = denrdif$x[which.max(denrdif$y)]
    c(mu=mu, nrdif=nrdif)
  })
  # smooth mode with spline, weighted by number of events and importance (idr-based snr)
  smsp.mu = smooth.spline(x=testqc, y=testclus['mu',], w=testclus['nrdif',]*compute.p.a.given.r(testqc), df=20)
  
  # Compute P(Bi| Ai, Ri) with the center mode location fixed at the smoothed locations.
  points(allqc, (predict(smsp.mu, allqc))$y, col='orange')
  par(mfrow=c(2,2))
  allclusters = lapply(allqc, function(qc){
    print(qc)
    rc = qc*npiq
    del = 0.05*npiq
    iblock = which(abs(tf.kc1.srk-rc)<del & abs(tf.kc3.srk-rc)<del)
    rdif = (tf.gc1.srk[iblock] + tf.gc3.srk[iblock] - tf.kc1.srk[iblock] - tf.kc3.srk[iblock]) / 2
    denrdif = density(rdif)
    plot(denrdif, main=paste('P(Bi | Ai, Ri), Ri =', qc))
    
    # EM to find 3 clusters. 1 reproducible, 2 left irreproducible, 3 right irreproducible
    # initiate EM
    nrdif = length(rdif)
    unsmoothed.mu = denrdif$x[which.max(denrdif$y)]
    mu = (predict(smsp.mu, qc))$y; sig = IQR(rdif); c2 = (sum(rdif<mu-2*sig)+1)/nrdif; c3 = (sum(rdif>mu+2*sig)+1)/nrdif; c1=1-c2-c3;
    dens2 = 1/(-min(rdif)); dens3 = 1/max(rdif);
    # iterate EM
    niter = 700
    for (i in 1:niter) {
      # assign cluster responsibility
      resp2 = c2*dens2/(c2*dens2+c1*dnorm(rdif, mu, sig)) * (rdif<mu)
      resp3 = c3*dens3/(c3*dens3+c1*dnorm(rdif, mu, sig)) * (rdif>mu)
      resp1 = 1 - resp2 - resp3
      # infer distribution params
      c2 = sum(resp2)/nrdif
      c3 = sum(resp3)/nrdif
      c1 = 1-c2-c3
      # mu = sum(resp1*rdif)/sum(resp1)
      sig = sqrt(sum(resp1*(rdif-mu)^2)/sum(resp1))
    }
    plx = seq(min(rdif),max(rdif),length.out=50)
    points(plx, c1*dnorm(plx,mu,sig), type='l', col='red')
    points(c(min(rdif),min(rdif),mu,mu), c(0,1,1,0)*c2/(-min(rdif)), type='l', col='blue')
    points(c(mu,mu, max(rdif),max(rdif)), c(0,1,1,0)*c3/(max(rdif)), type='l', col='green')
    c(c1=c1, c2=c2, c3=c3, mu=mu, sig=sig, dens2=dens2, dens3=dens3, unsmoothed.mu=unsmoothed.mu)
  })
  allclus=do.call(rbind,allclusters)

  par(mfrow=c(1,1))

  ## Combine P(Ai| Ri) and P(Bi| Ai, Ri) and compute decision bourndaries
  tf.kc.srk = (tf.kc1.srk+tf.kc3.srk)/2
  tf.gc.srk = (tf.gc1.srk+tf.gc3.srk)/2
  
  condprob = lapply(1:length(allqc), function(i){
    x=(1:npiq)-allqc[i]*npiq
    a=allclus[i,'c2']*allclus[i,'dens2']*(x<0)
    b=allclus[i,'c1']*dnorm(x,allclus[i,'mu'],allclus[i,'sig'])
    c=a/(a+b)
  })
  fullprob = lapply(1:length(allqc), function(i){
    condprob[[i]]*p.a.given.r[i]
  })
  calllevels = c(1-1e-6, 1-1e-5, .99995, .9999, .9997, .9995, .999, .997, .995, .993, .99, .98, .97, .96, .95, .93, .9, .8, .7, .6, .5, .4, .3, .2, .1, .05, .02, .01)
  frontiers = lapply(calllevels, function(l){
    frnt = sapply(fullprob, function(pvec){
      which(pvec<l)[1]-1
    })
    lf = length(frnt); for (i in lf:1) {
      aa = setdiff(which(frnt > frnt[i]), lf:i)
      frnt[aa] = frnt[i]
    }
    return(frnt)
  })
  callsets.levels = lapply(calllevels, function(ll){
    called = lapply(1:length(allqc), function(i){
      if (allqc[i]==max(allqc)) {
        u=npiq
        l = npiq*(allqc[i]+allqc[i-1])/2
      } else if (allqc[i]==min(allqc)) {
        u = npiq*(allqc[i]+allqc[i+1])/2
        l = 0
      } else {
        u = npiq*(allqc[i]+allqc[i+1])/2
        l = npiq*(allqc[i]+allqc[i-1])/2
      }
      which(tf.kc.srk<u & tf.kc.srk>l & tf.gc.srk<=frontiers[[which(calllevels==ll)]][i])
    })
    do.call('c', called)
  })
  # Generate PR plots
  basecuts = quantile(tf.kc1$shape-tf.gc1$shape, probs=seq(1,0,length.out=30))
  callcombp2 = lapply(basecuts, function(bc){
    aa = which(tf.kc1$shape-tf.gc1$shape > bc)
    union(aa, callsets.levels[[length(callsets.levels)]])
  })
  callcombp2 = callcombp2[sapply(callcombp2, length) > 2*length(callsets.levels[[length(callsets.levels)]])]
  callsets.comb = c(callsets.levels, callcombp2)
  
  plot(tf.gc.srk, tf.kc.srk, pch='.', xlab='GM12878 PIQ rank', ylab='K562 PIQ rank', main=paste(tfstr, 'decision boundary'))
  points(tf.gc.srk[gs.edgerqdiff.e2.piqidx], tf.kc.srk[gs.edgerqdiff.e2.piqidx], col='red', pch='*')
  for (i in c(8,9,10,12,14,16,19,22,25,28)){
    points(frontiers[[i]], allqc*npiq, type='l', col='orange', lwd=2)
  }
  
  ## Calculate PR and ROC metrics
  source('compute_metrics.r')
  dev.off()
  
  source('write_outputs.r')